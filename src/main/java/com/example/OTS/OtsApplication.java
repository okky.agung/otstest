package com.example.OTS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class OtsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtsApplication.class, args);
	}

}
