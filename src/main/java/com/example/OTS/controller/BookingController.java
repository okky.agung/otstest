package com.example.OTS.controller;

import com.example.OTS.model.BookingCode;
import com.example.OTS.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
public class BookingController {

    @Autowired
    BookingRepository bookingRepository;



    @RequestMapping(value = "/getBooking")
    public List<BookingCode> getBooking() {
        List<BookingCode> book = bookingRepository.findAll();
        return book;
    }
    
}
