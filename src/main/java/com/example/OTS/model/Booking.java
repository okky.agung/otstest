package com.example.OTS.model;


import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "images_okky")
@EntityListeners(AuditingEntityListener.class)
public class Booking {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private int idrequestbooking;

    @NotBlank
    private int description;

    public int getIdrequestbooking() {
        return idrequestbooking;
    }

    public void setIdrequestbooking(int idrequestbooking) {
        this.idrequestbooking = idrequestbooking;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }
}
