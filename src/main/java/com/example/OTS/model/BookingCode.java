package com.example.OTS.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "okky")
@EntityListeners(AuditingEntityListener.class)
public class BookingCode {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
    private int idrequestbooking;

    @NotBlank
    private int id_platform;

    @NotBlank
    private String nama_platform;

    @NotBlank
    private String doc_type;

    @NotBlank
    private String term_of_payment;

    public int getIdrequestbooking() {
        return idrequestbooking;
    }

    public void setIdrequestbooking(int idrequestbooking) {
        this.idrequestbooking = idrequestbooking;
    }

    public int getId_platform() {
        return id_platform;
    }

    public void setId_platform(int id_platform) {
        this.id_platform = id_platform;
    }

    public String getNama_platform() {
        return nama_platform;
    }

    public void setNama_platform(String nama_platform) {
        this.nama_platform = nama_platform;
    }

    public String getDoc_type() {
        return doc_type;
    }

    public void setDoc_type(String doc_type) {
        this.doc_type = doc_type;
    }

    public String getTerm_of_payment() {
        return term_of_payment;
    }

    public void setTerm_of_payment(String term_of_payment) {
        this.term_of_payment = term_of_payment;
    }
}
