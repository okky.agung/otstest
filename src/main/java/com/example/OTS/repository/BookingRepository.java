package com.example.OTS.repository;

import com.example.OTS.model.BookingCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<BookingCode, Long> {
    List<BookingCode> findByStatusPeminjaman (int statusPeminjaman);
}